package net.dcog.renderfarm.client.java;

import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.*;

public class Uploader extends Thread
{
	private String server;
	private Client client;
	private ArrayList<String> jobnums = new ArrayList<String>();
	private ArrayList<String> filenames = new ArrayList<String>();
	private ArrayList<String> frames = new ArrayList<String>();
	private String os;
	private boolean stop = false;
	private JProgressBar progressBar;

	public Uploader(Client client, String server, String os)
	{
		this.client = client;
		this.server = server;
		this.os = os;
		progressBar = client.getUploadBar();
	}

	public void run()
	{
		while (true)
		{
			if(jobnums.isEmpty())
				try
				{
					if(stop != true)
					{
						client.log("No jobs. Sleeping for 2 seconds...", "upload");
						Thread.sleep(2000);
						continue;
					}
					else
						break;
				} catch (InterruptedException e)
				{
					client.printError(e);
				}
			try
			{
				String jobnum = jobnums.get(0);
				String filename = filenames.get(0);
				String frame = frames.get(0);
				client.log("Number of uploads left: " + jobnums.size(), "upload");
				client.log("Uploading file: " + filename, "upload");
				URL local = new URL(server + "/cupload");
				HttpURLConnection conn = (HttpURLConnection) local.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST"); // Triggers POST.
				String boundary = Long.toHexString(System.currentTimeMillis());
				conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

				
				StringBuffer buf = new StringBuffer();
				String CRLF = "\r\n";
				buf.append("--" + boundary).append(CRLF);
				buf.append("Content-Disposition: form-data; name=\"filename\"").append(CRLF);
				buf.append("").append(CRLF);
				if (os.equals("Windows"))
					buf.append(jobnum + filename.substring(filename.lastIndexOf('\\'))).append(CRLF);
				else
					buf.append(jobnum + filename.substring(filename.lastIndexOf('/'))).append(CRLF);
				buf.append("--" + boundary).append(CRLF);
				buf.append("Content-Disposition: form-data; name=\"idnum\"").append(CRLF);
				buf.append("").append(CRLF);
				buf.append(jobnum).append(CRLF);
				buf.append("--" + boundary).append(CRLF);
				buf.append("Content-Disposition: form-data; name=\"frame\"").append(CRLF);
				buf.append("").append(CRLF);
				buf.append(frame).append(CRLF);

				buf.append("--" + boundary).append(CRLF);
				if (os.equals("Windows"))
					buf.append(
							"Content-Disposition: form-data; name=\"image\"; filename=\"" + jobnum
									+ filename.substring(filename.lastIndexOf('\\')) + "\"").append(CRLF);
				else
					buf.append(
							"Content-Disposition: form-data; name=\"image\"; filename=\"" + jobnum
									+ filename.substring(filename.lastIndexOf('/')) + "\"").append(CRLF);
				buf.append("Content-Type: " + URLConnection.guessContentTypeFromName(filename)).append(CRLF);
				buf.append("").append(CRLF);
				String headers = buf.toString();
				
				buf = new StringBuffer();
				buf.append(CRLF);
				buf.append("--" + boundary + "--").append(CRLF);
				buf.append("").append(CRLF);
				String footers = buf.toString();

				DecimalFormat df = new DecimalFormat("#.##");
				df.setRoundingMode(java.math.RoundingMode.HALF_UP);
				long filesize = new File(filename).length();
				double fileLength = (double) filesize / 1024;
				client.log("File size to be uploaded: " + df.format(fileLength) + "kB", "upload");
				long requestLength = headers.length() + footers.length() +  filesize;
				conn.setFixedLengthStreamingMode((int) requestLength);
				
				conn.connect();
				
				DataOutputStream out = new DataOutputStream( conn.getOutputStream() );
				out.writeBytes(headers);
				out.flush();
				
				progressBar.setMinimum(0);
	            progressBar.setMaximum((int) filesize);
				
				int progress = 0;
				int bytesRead = 0;
				byte b[] = new byte[1024];
				BufferedInputStream bufin = new BufferedInputStream(new FileInputStream(filename));
				long starttime = System.currentTimeMillis();
				while ((bytesRead = bufin.read(b)) != -1)
				{
					out.write(b, 0, bytesRead);
					out.flush();
					progress += bytesRead;
					double sent = (double) progress / 1024;
					double time = (double) (System.currentTimeMillis()-starttime) / 1000;
					double speed = (double) sent / time;
					double timeleft = (double) (filesize-progress) / 1024 / speed;
					client.setProgressLabel(df.format(sent) + " kB of " + df.format(fileLength) + " kB at "
							+ df.format(speed) + " kB/s -- " + df.format(timeleft) + " seconds remaining");

					final int p = progress;
					SwingUtilities.invokeLater(new Runnable()
					{
						public void run()
						{
							progressBar.setValue(p);
							progressBar.revalidate();
						}
					});

				}
				out.writeBytes(footers);
				out.flush();
				out.close();

				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK)
				{
					client.log("Upload Successful.", "upload");
					File f = new File(filename);
					if(!f.delete())
						client.log("Temporary files could not be deleted. You should delete them manually.", "upload");
				}
				else
					client.log("Error with upload: " + conn.getResponseMessage(), "upload");
				jobnums.remove(0);
				filenames.remove(0);
				frames.remove(0);
				conn.disconnect();
			} catch (Exception e)
			{
				client.printError(e);
				e.printStackTrace();
			}
			finally
			{
			}
		}
		client.log("Upload thread stopped.", "upload");
	}

	public void addJob(String jobnum, String filename, String frame)
	{
		jobnums.add(jobnum);
		filenames.add(filename);
		frames.add(frame);
	}

	public void stopNicely()
	{
		client.log("Stopping upload thread...", "upload");
		stop = true;
	}
}
