package net.dcog.renderfarm.client.java;

import javax.swing.JButton;

public class RenderManager
{
	private Renderer render;
	private JButton start;
	private JButton stop;
	private Client client;

	RenderManager(JButton start, JButton stop, Client client)
	{
		this.start = start;
		this.stop = stop;
		this.client = client;
	}

	public void start(String server, String tmp, String blender, String os)
	{
		if (blender == "")
		{
			client.log("No blender executable selected.", "program");
			return;
		}
		if (render == null || !render.isStopping())
		{
			client.log("Using Blender located at:\n" + blender, "program");
			render = new Renderer();
			render.setVariables(client, server, tmp, blender, os);
			render.start();
			start.setEnabled(false);
			stop.setEnabled(true);
		} else if (render.isStopping())
		{
			client.log("Please wait for client to fully stop before starting again.", "program");
		}
	}

	public void stop()
	{
		client.log("Stopping the client...", "program");
		render.stopThread();
		stop.setEnabled(false);
		start.setEnabled(true);
	}
}
