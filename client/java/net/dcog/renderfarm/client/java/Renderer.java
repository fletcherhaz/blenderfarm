package net.dcog.renderfarm.client.java;

import java.io.*;
import java.net.*;
import java.util.Enumeration;
import java.util.zip.*;

public class Renderer extends Thread
{
	private Client client;
	private String blender;
	private String tmp;
	private String server;
	private String os;
	private boolean stop = false;

	public void run()
	{
		Uploader uploader = new Uploader(client, server, os);
		uploader.start();
		while (stop != true)
		{
			try
			{
				URL local = new URL(server + "cinitial");
				URLConnection uc = local.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
				client.log("Successfully connected to server.", "program");
				String line = in.readLine();
				in.close();
				if (line.contains("No jobs"))
				{
					client.log(line + "", "program");
					Thread.sleep(5000);
					continue;
				}
				long startTime = System.currentTimeMillis();
				client.log(startTime + "", "program");
				client.log(line + "", "program");
				String jobnum = line.substring(0, line.indexOf(','));
				client.log("Working on job #" + jobnum, "program");
				String blendurl = line.substring(line.indexOf(',') + 1, line.length());
				String url = server + blendurl.substring(11);
				String blendname = line.substring(line.lastIndexOf('/') + 1, line.length());
				client.log("Getting blender file: " + url, "program");
				client.log('\n' + blendname, "program");
				BufferedInputStream input = new java.io.BufferedInputStream(new java.net.URL(url).openStream());
				FileOutputStream fos = new java.io.FileOutputStream(tmp + "/" + blendname);
				BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
				byte data[] = new byte[1024];
				int count;
				while ((count = input.read(data, 0, 1024)) != -1)
				{
					bout.write(data, 0, count);
				}
				bout.close();
				fos.close();
				input.close();
				if(blendname.endsWith(".zip"))
				{
					client.log("Detected zip file. Extracting...", "program");
					ZipFile zip = new ZipFile(tmp + "/" + blendname);
					Enumeration entries = zip.entries();
					while (entries.hasMoreElements())
					{
						ZipEntry entry = (ZipEntry) entries.nextElement();
						if (entry.isDirectory())
						{
							(new File(tmp + "/" + entry.getName())).mkdir();
							continue;
						}
						InputStream in2 = zip.getInputStream(entry);
						OutputStream out = new BufferedOutputStream(new FileOutputStream(tmp + "/" + entry.getName()));
						byte[] buffer = new byte[1024];
					    int len;
					    while((len = in2.read(buffer)) >= 0)
					      out.write(buffer, 0, len);
					    in2.close();
					    out.close();
					}
					zip.close();
				}
				blendname = blendname.substring(0, blendname.length()-4) + ".blend";
				client.log("Downloaded blender file.", "program");
				String frame = getFrame(jobnum);
				while (!frame.equals("done"))
				{
					client.log("Rendering frame " + frame, "program");
					// Add logfile funcitonality
					String imagename = blendname.substring(0, blendname.lastIndexOf('.'));
					String cmd = blender + " -b " + tmp + "/" + blendname + " -o //" + imagename
							+ "- -x 1 -f " + frame;
					Process p = Runtime.getRuntime().exec(cmd);
					client.log(cmd, "program");
					BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
					BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					String s = "";
					String uploadfile = "";
					while ((s = stdInput.readLine()) != null)
					{
						client.log(s, "blender");
						if (s.contains("Saved:"))
							if (!s.contains("C:\\") && !s.contains("/"))
								uploadfile = tmp + s.substring(s.indexOf(":") + 2, s.lastIndexOf("Time") - 1);
							else if (os.equals("Linux"))
								uploadfile = s.substring(s.indexOf("/"), s.lastIndexOf("Time") - 1);
							else
								uploadfile = s.substring(s.indexOf("C:"), s.lastIndexOf("Time") - 1);
					}
					while ((s = stdError.readLine()) != null)
						client.log(s, "blender");
					client.log("Finished rendering frame: " + frame, "program");
					client.log("Uploading rendered image: " + uploadfile, "program");
					uploader.addJob(jobnum, uploadfile, frame);
					if (stop == true)
						break;
					frame = getFrame(jobnum);
				}
				long endTime = System.currentTimeMillis();
				client.log((endTime - startTime) + "", "program");
			} catch (Exception e)
			{
				client.printError(e);
			}
		}
		try
		{
			client.log("Waiting for uploader to finish...", "program");
			uploader.stopNicely();
			uploader.join();
		} catch (InterruptedException e)
		{
			client.printError(e);
		}
		client.log("Client Stopped.", "program");
		stop = false;
	}

	private String getFrame(String jobnum)
	{
		try
		{
			URL local = new URL(server + "/cframe?idnum=" + jobnum);
			URLConnection uc = local.openConnection();
			uc.setDoOutput(true); // Triggers POST.
			uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;");
			uc.setRequestProperty("Accept", "text/plain");
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			String line = in.readLine();
			client.log("Receieved frame to render from server: " + line, "program");
			in.close();
			return line;
		} catch (Exception e)
		{
			client.printError(e);
		}
		return "done";
	}

	public void setVariables(Client client, String server, String tmp, String blender, String os)
	{
		this.client = client;
		this.server = server;
		this.tmp = tmp;
		this.blender = blender;
		this.os = os;
	}

	public void stopThread()
	{
		stop = true;
	}

	public boolean isStopping()
	{
		return stop;
	}
}