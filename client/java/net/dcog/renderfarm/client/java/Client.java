package net.dcog.renderfarm.client.java;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Client extends JFrame implements WindowListener
{

	// If error occurs reset frame counter on server
	// Fix stop so it does not wait for sleep to finish. don't use Thread.sleep.
	// have a for loop that counts
	// Then just set counter to above the loop so it exits
	// Fix the url of the blendfile returned by cherrypy so python and java are
	// the same
	// Support bucket rendering
	// Support non packed rendering
	// Support using multiple cpus use n-1 where n is number of cpus
	// Better logfiles
	// Support mac
	private JTextArea programoutput = new JTextArea(15, 30);
	private JTextArea uploadoutput = new JTextArea(15, 30);
	private JTextArea blenderoutput = new JTextArea(15, 30);
	private RenderManager man;
	private JTextField tmploc = new JTextField("Select temporary folder for images....");
	private JComboBox os = new JComboBox(new String[] { "Windows", "Linux" });
	private BufferedWriter log = null;
	private JProgressBar uploadbar = new JProgressBar(0, 100);;
	private JLabel speed = new JLabel("");

	public Client()
	{
		super("Fletcher's Renderfarm Client");

		programoutput.setEditable(false);
		((DefaultCaret) programoutput.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		uploadoutput.setEditable(false);
		((DefaultCaret) uploadoutput.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		blenderoutput.setEditable(false);
		((DefaultCaret) blenderoutput.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		uploadbar.setEnabled(false);
		uploadbar.setStringPainted(true);

		JPanel top_blender = new JPanel();
		top_blender.setLayout(new BorderLayout());
		JLabel execlabel = new JLabel("Please select the blender executable:");
		execlabel.setAlignmentX(CENTER_ALIGNMENT);
		top_blender.add(execlabel, BorderLayout.NORTH);
		final JTextField blenderloc = new JTextField("Select the 'blender' file....");
		blenderloc.setEditable(false);
		top_blender.add(blenderloc, BorderLayout.CENTER);
		JButton browse = new JButton("Browse");
		browse.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					blenderloc.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		top_blender.add(browse, BorderLayout.EAST);
		JPanel ospanel = new JPanel();
		ospanel.setLayout(new BorderLayout());
		ospanel.add(new JLabel("Select your Operating System:"), BorderLayout.NORTH);
		ospanel.add(os);
		top_blender.add(ospanel, BorderLayout.SOUTH);

		JPanel mid_blender = new JPanel();
		mid_blender.setLayout(new BorderLayout());
		JLabel serverlabel = new JLabel("Please select the server (include /renderfarm/):");
		serverlabel.setAlignmentX(CENTER_ALIGNMENT);
		mid_blender.add(serverlabel, BorderLayout.NORTH);
		final JTextField serverloc = new JTextField("http://www.dcog.net/renderfarm/");
		serverloc.setEditable(true);
		mid_blender.add(serverloc, BorderLayout.CENTER);

		JPanel bot_blender = new JPanel();
		bot_blender.setLayout(new BorderLayout());
		JLabel tmplabel = new JLabel("Please select the folder to store temporary files in:");
		tmplabel.setAlignmentX(CENTER_ALIGNMENT);
		bot_blender.add(tmplabel, BorderLayout.NORTH);
		tmploc.setEditable(false);
		bot_blender.add(tmploc, BorderLayout.CENTER);
		JButton tmpbrowse = new JButton("Browse");
		tmpbrowse.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					tmploc.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		bot_blender.add(tmpbrowse, BorderLayout.EAST);

		JPanel buttons = new JPanel();
		final JButton start = new JButton("Start Client");
		final JButton stop = new JButton("Stop Client");
		buttons.add(start);
		stop.setEnabled(false);
		buttons.add(stop);

		JPanel main = new JPanel();
		main.setLayout(new BorderLayout());
		JPanel blender = new JPanel();
		blender.setLayout(new BorderLayout());
		blender.add(top_blender, BorderLayout.NORTH);
		blender.add(bot_blender, BorderLayout.CENTER);
		blender.add(mid_blender, BorderLayout.SOUTH);
		main.add(blender, BorderLayout.CENTER);
		main.add(buttons, BorderLayout.SOUTH);
		add(main, BorderLayout.NORTH);

		JScrollPane outputscroll = new JScrollPane(programoutput);
		outputscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		outputscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		JScrollPane uploadscroll = new JScrollPane(uploadoutput);
		uploadscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		uploadscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		JScrollPane blenderscroll = new JScrollPane(blenderoutput);
		blenderscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		blenderscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		JPanel upload = new JPanel();
		upload.setLayout(new BorderLayout());
		upload.add(uploadscroll, BorderLayout.CENTER);
		JPanel progress = new JPanel();
		progress.add(uploadbar);
		progress.add(speed);
		upload.add(progress, BorderLayout.SOUTH);

		JSplitPane rightSide = new JSplitPane(JSplitPane.VERTICAL_SPLIT, upload, blenderscroll);
		final JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, outputscroll, rightSide);
		add(split, BorderLayout.CENTER);
		split.setDividerLocation(0.5);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		setLocationRelativeTo(null);
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		man = new RenderManager(start, stop, this);
		start.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				split.setDividerLocation(0.5);
				try
				{
					if(log == null)
						log = new BufferedWriter(new FileWriter(tmploc.getText() + "/logfile"));
				} catch (Exception e)
				{
					printError(e);
				}
				man.start(serverloc.getText(), tmploc.getText(), blenderloc.getText(), os.getSelectedItem().toString());
			}
		});
		stop.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				man.stop();
			}
		});
		// tmploc.setText("/home/fletcher/blender/client/");
		// tmploc.setText("C:\\Users\\fletcher\\Desktop\\tmp");
		// serverloc.setText("http://192.168.0.101/renderfarm/");
		// blenderloc.setText("/home/fletcher/blender/versions/blender-2.56a-beta-linux-glibc27-x86_64/blender");
		// blenderloc.setText("C:\\Users\\fletcher\\Desktop\\blender-2.55-beta-windows64\\blender.exe");
	}


	public static void main(String args[])
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				new Client();
			}
		});
	}

	public void printError(Exception e)
	{
		man.stop();
		StringWriter error = new StringWriter();
		e.printStackTrace(new PrintWriter(error));
		log(error.toString() + "A serious error occured.\n", "error");
		log("Please contact Fletcher (fletcher.haz@gmail.com) and attach the file:\n", "error");
		if (os.getSelectedItem().toString().equals("Windows"))
			log(tmploc.getText() + "\\logfile\n", "error");
		else
			log(tmploc.getText() + "/logfile\n", "error");
	}

	public void log(String msg, String textarea)
	{
		try
		{
			log.write(textarea.toUpperCase() + ": " + msg + "\n");
			if(textarea.equals("blender"))
				blenderoutput.append(msg + "\n");
			else if(textarea.equals("upload"))
				uploadoutput.append(msg + "\n");
			else
				programoutput.append(msg + "\n");
		} catch (Exception e)
		{
			printError(e);
		}
	}

	public JProgressBar getUploadBar()
	{
		return uploadbar;
	}

	public void setProgressLabel(String msg)
	{
		speed.setText(msg);
	}

	public void windowClosing(WindowEvent e)
	{
		try
		{
			log.close();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}
	public void windowOpened(WindowEvent e){}
	public void windowClosed(WindowEvent e){}
	public void windowIconified(WindowEvent e){}
	public void windowDeiconified(WindowEvent e){}
	public void windowActivated(WindowEvent e){}
	public void windowDeactivated(WindowEvent e){}
}