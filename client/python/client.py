#!/usr/bin/python

import sys
import os
import httplib
import urllib
import subprocess
import mimetypes
import shutil

host = "192.168.0.4:8080"

def main():
    if not len(sys.argv) == 2:
        print "Please provide one argument with the location of your blender program."
    blender = sys.argv[1]
    cherrypy = httplib.HTTPConnection(host)
    cherrypy.request("GET", "/renderfarm/cinitial")
    res = cherrypy.getresponse().read()
    if res.find("No jobs") > -1:
        print res
        sys.exit(0)
    oformat = res[res.find(',')+1:res.rfind(',')]
    blendurl = res[res.rfind(',')+1:]
    blendname = blendurl[blendurl.rfind('/')+1:len(blendurl)-6]
    idnum = res[:res.find(',')]
    print "Getting blender file: " + blendurl
    nginx = httplib.HTTPConnection(host)
    nginx.request("GET", blendurl)
    blendres = nginx.getresponse()
    if not os.path.isdir(idnum):
        os.mkdir(idnum)
    blendfile = open(idnum + blendurl[blendurl.rfind('/'):], 'w')
    blendfile.write(blendres.read())
    blendfile.close()
    nginx.close()
    print "Running blender"
    runblender(blender, idnum, oformat, blendname, blendfile.name)
    shutil.rmtree(idnum)

def runblender(blender, idnum, oformat, blendname, blendfile):
    frame = postcherrypy(idnum, "cframe")
    while frame != "done":
        print "Rendering frame " + frame
        logfile = open(idnum + "/" + idnum + "-" + frame + ".log", 'w')
        cmd = [os.path.abspath(blender + '/blender'), '-b', os.path.abspath(blendfile), '-o', '//' + blendname + '-', '-F', oformat, '-x 1', '-f', frame]
        print ' '.join(cmd)
        process = subprocess.Popen(cmd, stdout=logfile, stderr=logfile)
        process.wait()
        logfile.close()
        print "Finished rendering frame " + frame
        print "Uploading rendered image"
        logfile = open(idnum + "/" + idnum + "-" + frame + ".log", 'r')
        filename = idnum
        for line in logfile.readlines():
            if line.find('Saved:') > -1:
                filename += line[line.rfind('/'):line.rfind('Time')-1]
        print post_multipart([('filename', filename), ('idnum', idnum)], [('image', filename, open(filename, 'rb').read())])
        frame = postcherrypy(idnum, "cframe")

def post_multipart(fields, files):
    content_type, body = encode_multipart_formdata(fields, files)
    h = httplib.HTTPConnection(host)
    headers = {
        'Content-Type': content_type
        }
    h.request('POST', '/renderfarm/cupload', body, headers)
    res = h.getresponse()
    return res.read()

def encode_multipart_formdata(fields, files):
    """
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return (content_type, body) ready for httplib.HTTP instance
    """
    BOUNDARY = '----------ThIs_Is_tHe_bouNdaRY_$'
    CRLF = '\r\n'
    L = []
    for (key, value) in fields:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"' % key)
        L.append('')
        L.append(value)
    for (key, filename, value) in files:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename))
        L.append('Content-Type: %s' % get_content_type(filename))
        L.append('')
        L.append(value)
    L.append('--' + BOUNDARY + '--')
    L.append('')
    body = CRLF.join(L)
    content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
    return content_type, body

def get_content_type(filename):
    return mimetypes.guess_type(filename)[0] or 'application/octet-stream'

def postcherrypy(idnum, url):
    params = urllib.urlencode({'idnum': idnum})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    cherrypy = httplib.HTTPConnection(host)
    cherrypy.request("POST", "/renderfarm/" + url, params, headers)
    retval = cherrypy.getresponse().read()
    cherrypy.close()
    return retval

if __name__ == '__main__':
    sys.exit(main())
