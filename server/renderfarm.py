import cherrypy
import os
import mysql.connector
import shutil
from shutil import copyfileobj

farmloc = "/home/fletcher/git/renderfarm/server"

class RenderFarm(object):

    _cp_config = {
        'tools.staticdir.on' : True,
        'tools.staticdir.dir' : farmloc + '/static',
        'tools.staticdir.index' : 'index.html',
    }

    def createjob(self, blendfile, start, final, dpasswd, rpasswd):
        # Add priority argument and change return statment to include it
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        priority = 0
        name = blendfile.filename
        sql = 'INSERT INTO jobs (blend_file, start_frame, next_frame, final_frame, priority, finished, dpasswd) VALUES (\'' + name + '\',\'' + start + '\',\'' + start + '\',\'' + final + '\',\'' + str(priority) + '\', 0, \'' + dpasswd + '\');'
        cursor.execute(sql)
        cherrypy.log(sql)
        sql = 'SELECT MAX(id) FROM jobs;'
        cherrypy.log(sql)
        cursor.execute(sql)
        row = cursor.fetchone()
        nextid = row[0]
        files = []
        pdir = farmloc + "/static/projects/"
        if not os.path.exists(pdir):
            # Remove the slash to check if its a file and remove it
            if os.path.isfile(pdir[:len(pdir)-1]):
                os.remove(pdir[:len(pdir)-1])
            os.mkdir(pdir)
        for f in os.listdir(pdir):
            if not f[0] == '.':
                files.append(f)
        dirname = pdir + str(nextid)
        os.mkdir(dirname)
        os.mkdir(dirname + '/output')
        os.mkdir(dirname + '/output/stills')
        f = open(dirname + '/' + name, 'wb')
        data = blendfile.file
        data.seek(0)   # just to ensure we're at the beginning
        copyfileobj(fsrc=data, fdst=f, length=1024 * 8)
        f.close()
        conn.close()
        return "<html><head><title>Fletcher's Renderfarm</title></head><body>Saved " + name + " successfully.<br>Starting job number " + str(nextid) + ".<br>Frames " + start + " to " + final + ".<br>Click <a href='/renderfarm/'>here</a> to return to the main page.</body></html>"
    createjob.exposed = True

    def deletejob(self, idnum, dpasswd):
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT id,blend_file,dpasswd FROM jobs WHERE id=\'' + idnum + '\';'
        cursor.execute(sql)
        row = cursor.fetchone()
        if row == None:
            text = 'Could not find this job in the joblist.<br>Click <a href="/renderfarm/viewjobs">here</a> to return to the job list.<br>'
        else:
            if dpasswd != row[2]:
                return 'Permission denied'
            sql = 'UPDATE jobs SET finished=\'1\' WHERE id=\'' + str(row[0]) + '\';'
            cursor.execute(sql)
            # Add in more info about deleted job
            text = "Deleted job " + row[1]
            text += "<br>Although this job will no longer render any more frames. The frames already rendered are still available."
            text += '<br>Click <a href="/renderfarm/viewdir?idnum=' + str(row[0]) + '&name=">here</a> to view them for this job.'
            text += '<br>Or click <a href="/renderfarm/viewjobs">here</a> to got back to the job list.'
        conn.close()
        return text
    deletejob.exposed = True

    def viewjobs(self):
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT * FROM jobs WHERE finished=\'0\';'
        cursor.execute(sql)
        text = "<html><head><title>Fletcher's Renderfarm</title>\n"
        text += "<script language='javascript'>\n"
        text += "function deletejob(idnum)\n"
        text += "{\n"
        text += "   var pass = prompt(\"Enter Password to Stop Project \" + idnum + \"'s files:\")\n"
        text += "   if (pass != null)\n"
        text += "   location.href='/renderfarm/deletejob?idnum=' + idnum + '&dpasswd=' + pass\n"
        text += "}\n"
        text += "</script>\n"
        text += "</head><body><table>"
        row = cursor.fetchone()
        while row != None:
            text += "<tr><td>" + str(row[0]) + "&nbsp;&nbsp;</td><td>Blender Filename: </td><td>" + row[1] + '</td><td><input type="button" value="Delete Job" onClick=deletejob(\'' + str(row[0]) + '\')></td></tr>\n'
            text += "<tr><td></td><td>Start Frame:</td><td>" + str(row[2]) + "</td></tr>"
            text += "<tr><td></td><td>Next Frame to Render:</td><td>" + str(row[3]) + "</td></tr>"
            text += "<tr><td></td><td>Final Frame:</td><td>" + str(row[4]) + "</td></tr><tr><td><br></td></tr>"
            row = cursor.fetchone()
        if text[text.rfind('<'):] == "<table>":
            text += "<tr><td></td><td>No jobs currently active.</td></tr>"
        text += "<tr><td></td><td><br>Click <a href='/renderfarm/'>here</a> to return to the main page.</td></tr>"
        text += "</table></body></html>"
        conn.close()
        return text
    viewjobs.exposed = True

    def viewdir(self, idnum, name):
        text = "<html><head><title>Fletcher's Renderfarm</title></head><body>"
        dirpath = farmloc + '/static/projects/' + idnum + '/output/' + name
        cherrypy.log('viewdir has a request for: ' + dirpath)
        if os.path.exists(dirpath):
            files = os.listdir(dirpath)
            for f in files:
                filepath = farmloc + '/static/projects/' + idnum + '/output/' + name + "/" + f
                if os.path.isfile(filepath):
                    text += "<a href='/renderfarm/projects/" + idnum + "/output/" + name + "/" + f + "'>" + f + "</a><br>"
                else:
                    text += "<a href='/renderfarm/viewdir?idnum=" + idnum + "&name=" + name + "/" + f + "'>" + f + "</a><br>"
        else:
            text += "This project has no files."
        if name == "":
            text += "<br>Click <a href='/renderfarm/viewfinished'>here</a> to return to the finished jobs page."
        else:
            text += "<br>Click <a href='/renderfarm/viewdir?idnum=" + idnum + "&name=" + name[:name.rfind('/')] + "'>here</a> to go back one page."
        text += "</body></html>"
        return text
    viewdir.exposed = True

    def viewfinished(self):
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT * FROM jobs WHERE finished=\'1\';'
        cursor.execute(sql)
        text = "<html><head><title>Fletcher's Renderfarm</title>"
        text += "<script language='javascript'>\n"
        text += "function deletefiles(idnum)\n"
        text += "{\n"
        text += "   var pass = prompt(\"Enter Password to Delete Project \" + idnum + \"'s files:\")\n"
        text += "   if (pass != null)\n"
        text += "   location.href='/renderfarm/deletefiles?idnum=' + idnum + '&dpasswd=' + pass\n"
        text += "}\n"
        text += "function createmovie(idnum)\n"
        text += "{\n"
        text += "   location.href='/renderfarm/createmovie?idnum=' + idnum\n"
        text += "}\n"
        text += "function viewfiles(idnum)"
        text += "{"
        text += "   location.href='/renderfarm/viewdir?idnum=' + idnum + '&name='"
        text += "}"
        text += "</script>"
        text += "</head><body><table>"
        row = cursor.fetchone()
        while row != None:
            text += "<tr><td>" + str(row[0]) + "&nbsp;&nbsp;</td><td>Blender Filename: </td><td>" + str(row[1]) + '</td><td><input type="button" value="View Files" onClick=viewfiles(\'' + str(row[0]) + '\')></td></tr>\n'
            text += "<tr><td></td><td>Start Frame:</td><td>" + str(row[2]) + '</td><td><input type="button" value="Create Movie" onClick=createmovie(\'' + str(row[0]) + '\')></td></tr>'
            text += '<tr><td></td><td>Next Frame to Render:</td><td>' + str(row[3]) + '</td><td><input type="button" value="Delete Files" onClick=deletefiles(\'' + str(row[0]) + '\')></td></tr>'
            text += "<tr><td></td><td>Final Frame:</td><td>" + str(row[4]) + "</td></tr><tr><td><br></td></tr>"
            row = cursor.fetchone()
        if text[text.rfind('<'):] == "<table>":
            text += 'No finished jobs.<br>'
        text += '</table><br>Click <a href="/renderfarm/">here</a> to return to the main page.'
        text += "</body></html>"
        conn.close()
        return text
    viewfinished.exposed = True

    def deletefiles(self, idnum, dpasswd):
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT id,blend_file,dpasswd FROM jobs WHERE id=\'' + idnum + '\';'
        cursor.execute(sql)
        row = cursor.fetchone()
        if row == None:
            text = 'Could not find this job in the joblist.<br>Click <a href="/renderfarm/viewfinished">here</a> to return to the job list.<br>'
        else:
            if dpasswd != row[2]:
                text = 'Permission denied'
            else:
                sql = 'DELETE FROM jobs WHERE id=\'' + idnum + '\';'
                cursor.execute(sql)
                shutil.rmtree(farmloc + '/static/projects/' + idnum)
                text = "<html><head><title>Fletcher's Renderfarm</title></head><body>Successfully removed project " + idnum + ".<br>Click <a href='/renderfarm/viewfinished'>here</a> to return to the view finished jobs page.</body></html>"
        return text
    deletefiles.exposed = True

    def createmovie(self, idnum):
        full = os.listdir(farmloc + '/static/projects/' + idnum + '/output/stills/')[0]
        ending = full[full.rfind('.'):]
        name = full[:full.find('-')+1]
        num = len(full) - (len(name) + len(ending))
        cmd = ['ffmpeg', '-i', farmloc + '/static/projects/' + idnum + '/output/stills/' + name + '%0' + str(num) + 'd' + ending, farmloc + '/static/projects/' + idnum + '/output/output.avi']
        return ' '.join(cmd)
    createmovie.exposed = True

    # This is the first time the client connects
    def cinitial(self):
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT id,blend_file FROM jobs WHERE finished=\'0\';'
        cursor.execute(sql)
        row = cursor.fetchone()
        conn.close()
        if row == None:
            return "No jobs currently running"
        return str(row[0]) + ",/renderfarm/projects/" + str(row[0]) + "/" + row[1]
    cinitial.exposed = True

    # This is the rest of the times the client connects
    # Checks what blend file this host is using
    # Checks the next frame for this file
    # Returns that frame number
    def cframe(self, idnum):
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT * FROM jobs WHERE finished=\'0\';'
        cursor.execute(sql)
        row = cursor.fetchone()
        cherrypy.log('Getting a new frame number for project: ' + idnum)
        if row == None:
            nextframe = 'done'
        else:
            if int(row[3]) > int(row[4]):
                nextframe = 'done'
            else:
                nextframe = row[3]
                sql = 'UPDATE jobs SET next_frame=\'' + str(int(nextframe)+1) + '\' WHERE id=\'' + str(idnum) + '\';'
                cursor.execute(sql)
        conn.close()
        return nextframe
    cframe.exposed = True

    def cupload(self, filename, idnum, image, frame):
        cherrypy.log('Saving file: ' + filename)
        f = open(farmloc + '/static/projects/' + idnum + '/output/stills/' + filename[filename.find('/')+1:], 'wb')
        data = image.file
        data.seek(0)   # just to ensure we're at the beginning
        copyfileobj(fsrc=data, fdst=f, length=1024 * 8)
        f.close()
        conn = self.createConn()
        if isinstance(conn, str):
            return error(conn)
        cursor = conn.cursor()
        sql = 'SELECT final_frame FROM jobs WHERE id=\'' + str(idnum) + '\';'
        cherrypy.log(sql)
        cursor.execute(sql)
        row = cursor.fetchone()
        cherrypy.log(frame)
        if row != None:
            if int(row[0]) == int(frame):
                sql = 'UPDATE jobs SET finished=\'1\' WHERE id=\'' + str(idnum) + '\';'
                cursor.execute(sql)
    cupload.exposed = True

    def createConn(self):
        try:
            conn = MySQLdb.connect( host    = "localhost",
                                    user    = "renderfarm",
                                    passwd  = "render901?",
                                    db      = "renderfarm")
        except MySQLdb.Error as e:
            conn = "Error %d: %s" % (e.args[0], e.args[1])
        return conn

    def error(msg):
        cherrypy.log(msg)
        html = createHeader() + '</head><body>An error occured. Please contact the site administrator Fletcher Hazlehurst (fletcher.haz@gmail.com) and attache the following message.<br><br>'
        html += msg
        html += '</body></html>'
        return html

    def createHeader():
        html = "<html><head><title>Fletcher's Renderfarm</title>\n"
        return html

cherrypy.tree.mount(RenderFarm(), '/renderfarm')
